def read_grid(file_path):
    """Reads a grid of characters from a file."""
    with open(file_path, 'r') as file:
        return file.read().splitlines()

def find_numeric_starts_part1(grid):
    """Finds the starting points of numeric sequences adjacent to non-digit/non-dot characters."""
    numeric_starts = set()
    for row_idx, row in enumerate(grid):
        for col_idx, char in enumerate(row):
            if char.isdigit() or char == ".":
                continue
            for dr in range(row_idx - 1, row_idx + 2):
                for dc in range(col_idx - 1, col_idx + 2):
                    if 0 <= dr < len(grid) and 0 <= dc < len(grid[dr]) and grid[dr][dc].isdigit():
                        while dc > 0 and grid[dr][dc - 1].isdigit():
                            dc -= 1
                        numeric_starts.add((dr, dc))
    return numeric_starts

def extract_numbers_part1(grid, starts):
    """Extracts and returns all numeric sequences starting from given positions."""
    numbers = []
    for r, c in starts:
        number = ""
        while c < len(grid[r]) and grid[r][c].isdigit():
            number += grid[r][c]
            c += 1
        numbers.append(int(number))
    return numbers

def find_numeric_sequences_around_star(grid, row, col):
    """Finds numeric sequences around a given '*' character in the grid."""
    numeric_starts = set()
    for cr in [row - 1, row, row + 1]:
        for cc in [col - 1, col, col + 1]:
            if 0 <= cr < len(grid) and 0 <= cc < len(grid[cr]) and grid[cr][cc].isdigit():
                while cc > 0 and grid[cr][cc - 1].isdigit():
                    cc -= 1
                numeric_starts.add((cr, cc))
    return numeric_starts

def calculate_product_of_sequences(grid, numeric_starts):
    """Calculates the product of two numeric sequences."""
    if len(numeric_starts) != 2:
        return 0
    numbers = []
    for cr, cc in numeric_starts:
        sequence = ""
        while cc < len(grid[cr]) and grid[cr][cc].isdigit():
            sequence += grid[cr][cc]
            cc += 1
        numbers.append(int(sequence))
    return numbers[0] * numbers[1]

def main(file_path):
    grid = read_grid(file_path)

    # Part 1: Sum of Numeric Sequences
    numeric_starts_part1 = find_numeric_starts_part1(grid)
    numbers_part1 = extract_numbers_part1(grid, numeric_starts_part1)
    sum_result = sum(numbers_part1)

    # Part 2: Product of Numeric Sequences Around '*'
    total_product = 0
    for r, row in enumerate(grid):
        for c, char in enumerate(row):
            if char != "*":
                continue
            numeric_starts_part2 = find_numeric_sequences_around_star(grid, r, c)
            total_product += calculate_product_of_sequences(grid, numeric_starts_part2)

    print("Sum of Part 1:", sum_result)
    print("Total Product of Part 2:", total_product)

if __name__ == "__main__":
    file_path = 'input.txt'
    main(file_path)
