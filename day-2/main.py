import re

def main(file_path="day-2/input.txt"):
    total_id = 0
    total_multiply = 0

    with open(file_path, "r") as file:
        for line in file:
            total_multiply += multiply_max_cubes(line)
            game_id = line.split(":")[0]
        
            if is_game_valid(line):
                total_id += get_game_id(game_id)
        



    print("Total (Part 1):",     total_id)
    print("Total (Part 2):",     total_multiply)

def get_game_id(line):
    id_digits = re.findall(r'\d', line)
    game_id = ''
    game_id = game_id.join(id_digits) 
    print(game_id)
    return int(game_id)

def get_cubes(line):
    # Get cube counts
    red_cubes = re.findall(r"(\d+) red", line)
    green_cubes = re.findall(r"(\d+) green", line)
    blue_cubes = re.findall(r"(\d+) blue", line)

    # Convert to int
    red_cubes = [int(item) for item in red_cubes]
    green_cubes = [int(item) for item in green_cubes]
    blue_cubes = [int(item) for item in blue_cubes]

    return red_cubes, green_cubes, blue_cubes

def is_game_valid(line):
    cubes = {"red": 12, "green": 13, "blue": 14}

    red_count, green_count, blue_count = get_max_cubes(line)

    if red_count > cubes["red"] or green_count > cubes["green"] or blue_count > cubes["blue"]:
        return False

    return True
  
def get_max_cubes(line):
    red_cubes, green_cubes, blue_cubes = get_cubes(line)
    return max(red_cubes), max(green_cubes), max(blue_cubes)

def multiply_max_cubes(line): 
    red_cubes, green_cubes, blue_cubes = get_max_cubes(line)
    return red_cubes * green_cubes * blue_cubes
  
if __name__ == "__main__":
    main()
