import re

def extract_calibration_value(file_path, replace_strings_with_digits=None):
    total = 0
    with open(file_path, "r") as file:
        for line in file:
            line = line.strip()

            if replace_strings_with_digits:
                line = replace_strings_with_digits(line)

            digits = re.findall(r"\d", line)
            if digits:
                number = int(digits[0] + digits[-1])
                total += number

    return total

def replace_strings_with_digits(line):
    numbers = {
        "one": "o1e",
        "two": "t2p",
        "three": "t3e",
        "four": "f4r",
        "five": "f5e",
        "six": "s6x",
        "seven": "s7n",
        "eight": "e8t",
        "nine": "n9e"
    }

    for spelled_number, digit in numbers.items():
        line = line.replace(spelled_number, digit)

    return line

def main():
    file_path = "day-1/input.txt"
    
    # Part 1:
    total_part1 = extract_calibration_value(file_path)
    print("Total (Part 1):", total_part1)

    # Part 2:
    total_part2 = extract_calibration_value(file_path, replace_strings_with_digits)
    print("Total (Part 2):", total_part2)

if __name__ == "__main__":
    main()
